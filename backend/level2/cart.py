from marshmallow import ValidationError

from common.cart import CartInterface
from common.schemas import CartResponseSchema


class CartOps(CartInterface):
    def total_by_quantity_price(self):
        carts = []

        articles = {article.id: article.price for article in self.articles}

        for cart in self.carts:

            total = sum(
                [articles[item.article_id] * item.quantity for item in cart.items]
            )

            try:
                result = CartResponseSchema().load(dict(id=cart.id, total=total))
            except ValidationError as err:
                raise err

            carts.append(result)

        return carts

    def apply_discount(self):
        pass

    def calculate_fees(self, carts):
        for cart in carts:
            temp_total = cart["total"]
            for item in self.fees:
                min_price = item["eligible_transaction_volume"]["min_price"]
                max_price = item["eligible_transaction_volume"]["max_price"]
                fee = item["price"]
                check_min = temp_total >= min_price
                check_max = True if max_price is None else temp_total < max_price
                if check_min and check_max:
                    temp_total += fee

            cart["total"] = temp_total

        return carts

    def total(self):
        carts = self.total_by_quantity_price()
        carts = self.calculate_fees(carts)

        return {"carts": carts}
