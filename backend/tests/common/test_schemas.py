import factory
import pytest
from marshmallow.fields import Integer, Str, Nested
from marshmallow import ValidationError

from common.models import Article, Item, Cart
from common.schemas import ArticleSchema, ItemSchema, CartSchema


class TestArticleSchema:
    def setup_method(self):
        self.payload = {}
        self.schema = ArticleSchema()

    def make_schema(self):
        try:
            return self.schema.load({"id": 1, "name": "John", "price": 199})
        except ValidationError as err:
            raise err

    def test_id_is_str_field(self):
        assert isinstance(ArticleSchema._declared_fields.get("id"), Integer) is True

    def test_name_is_str_field(self):
        assert isinstance(ArticleSchema._declared_fields.get("name"), Str) is True

    def test_price_is_str_field(self):
        assert isinstance(ArticleSchema._declared_fields.get("price"), Integer) is True

    def test_result_should_be_instance_of_article_model(self):
        result = self.make_schema()
        assert isinstance(result, Article) is True


class TestItemSchema:
    def setup_method(self):
        self.payload = {}
        self.schema = ItemSchema()

    def make_schema(self):
        try:
            return self.schema.load({"article_id": 1, "quantity": 5})
        except ValidationError as err:
            raise err

    def test_article_id_is_integer_field(self):
        assert (
            isinstance(ItemSchema._declared_fields.get("article_id"), Integer) is True
        )

    def test_article_quantity_is_integer_field(self):
        assert isinstance(ItemSchema._declared_fields.get("quantity"), Integer) is True

    def test_result_should_be_instance_of_item_model(self):
        result = self.make_schema()
        assert isinstance(result, Item) is True


class TestCartSchema:
    def setup_method(self):
        self.payload = {}
        self.schema = CartSchema()

    def make_schema(self):
        try:
            return self.schema.load(
                {"id": 1, "items": [{"article_id": 1, "quantity": 5}]}
            )
        except ValidationError as err:
            raise err

    def test_id_is_integer_field(self):
        assert isinstance(CartSchema._declared_fields.get("id"), Integer) is True

    def test_article_items_is_nested_field(self):
        assert isinstance(CartSchema._declared_fields.get("items"), Nested) is True

    def test_result_should_be_instance_of_item_model(self):
        result = self.make_schema()
        assert isinstance(result, Cart) is True
