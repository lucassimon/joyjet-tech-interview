import os
import json


from marshmallow import ValidationError
from common.schemas import ArticleSchema, CartSchema, FeeSchema
from level2.cart import CartOps


class TestCartOps:
    @classmethod
    def setup_class(cls):
        from_here = os.path.dirname(__file__)
        relative_path = "../../level2/data.json"
        path = os.path.join(from_here, relative_path)

        with open(path, "r") as data_file:
            cls.spec = json.load(data_file)
        data_file.close()

        relative_path = "../../level2/output.json"
        path = os.path.join(from_here, relative_path)

        with open(path, "r") as data_file:
            cls.output = json.load(data_file)

    def setup_method(self):

        try:
            self.articles = ArticleSchema(many=True).load(self.spec["articles"])
        except ValidationError as err:
            raise err

        try:
            self.carts = CartSchema(many=True).load(self.spec["carts"])
        except ValidationError as err:
            raise err

        try:
            self.fees = FeeSchema(many=True).load(self.spec["delivery_fees"])
        except ValidationError as err:
            raise err

        self.carts = CartOps(self.carts, self.articles, self.fees)
        self.response = self.carts.total()

    def test_should_first_cart_has_2000_total(self):
        received = self.response["carts"][0]
        expected = self.output["carts"][0]

        assert received == expected

    def test_should_second_cart_has_1800_total(self):
        received = self.response["carts"][1]
        expected = self.output["carts"][1]

        assert received == expected

    def test_should_third_cart_has_800_total(self):
        received = self.response["carts"][2]
        expected = self.output["carts"][2]

        assert received == expected
