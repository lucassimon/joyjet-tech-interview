from marshmallow import Schema, fields, post_load, validates, ValidationError

from . import models


class ArticleSchema(Schema):
    id = fields.Integer(required=True)
    name = fields.Str(required=True)
    price = fields.Integer(required=True)

    @post_load
    def make(self, data, **kwargs):
        return models.Article(**data)


class ItemSchema(Schema):
    article_id = fields.Integer(required=True)
    quantity = fields.Integer(required=True)

    @post_load
    def make(self, data, **kwargs):
        return models.Item(**data)


class CartSchema(Schema):
    id = fields.Integer(required=True)
    items = fields.Nested(ItemSchema, many=True)

    @post_load
    def make(self, data, **kwargs):
        return models.Cart(**data)


class CartResponseSchema(Schema):
    id = fields.Integer(required=True)
    total = fields.Integer(required=True)


class VolumeSchema(Schema):
    min_price = fields.Integer(required=True)
    max_price = fields.Integer(required=True, allow_none=True)


class FeeSchema(Schema):
    eligible_transaction_volume = fields.Nested(VolumeSchema)
    price = fields.Integer(required=True)


class DiscountSchema(Schema):
    article_id = fields.Integer(required=True)
    type = fields.Str(required=True)
    value = fields.Integer(required=True)
