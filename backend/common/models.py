class Article:
    def __init__(self, id, name, price):
        self.id = id
        self.name = name
        self.price = price

    def __repr__(self):
        return "<Artigo(name={self.name!r})>".format(self=self)


class Item:
    def __init__(self, article_id, quantity):
        self.article_id = article_id
        self.quantity = quantity

    def __repr__(self):
        return "<Item(article_id={self.article_id!r}, quantity={self.quantity})>".format(
            self=self
        )


class Cart:
    def __init__(self, id, items):
        self.id = id
        self.items = items

    def __repr__(self):
        return "<Cart(id={self.id!r})>".format(self=self)
