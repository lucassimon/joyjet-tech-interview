from abc import ABC, abstractmethod


class CartInterface(ABC):
    def __init__(self, carts, articles, fees=[], discounts=[]):
        self.carts = carts
        self.articles = articles

        if fees:
            self.fees = fees

        if discounts:
            self.discounts = discounts

    @abstractmethod
    def total_by_quantity_price(self):
        pass

    @abstractmethod
    def apply_discount(self):
        pass

    @abstractmethod
    def calculate_fees(self):
        pass

    @abstractmethod
    def total(self):
        pass
