from marshmallow import ValidationError

from common.cart import CartInterface
from common.schemas import CartResponseSchema


class CartOps(CartInterface):
    def total_by_quantity_price(self):
        carts = []
        discounts = []

        articles = {article.id: article.price for article in self.articles}

        for cart in self.carts:
            total = []
            total_discount = []
            for item in cart.items:
                total.append(articles[item.article_id] * item.quantity)

                total_discount.append(
                    self.calculate_discount(articles[item.article_id], item.article_id)
                    * item.quantity
                )

            try:
                result = CartResponseSchema().load(dict(id=cart.id, total=sum(total)))
            except ValidationError as err:
                raise err

            carts.append(result)
            discounts.append({"id": cart.id, "discount": sum(total_discount)})

        return carts, discounts

    def calculate_discount(self, price, article_id):
        discounts = {
            discount["article_id"]: (discount["type"], discount["value"])
            for discount in self.discounts
        }

        if article_id in discounts:
            kind = discounts[article_id][0]
            value = discounts[article_id][1]

            if kind == "amount":
                return value

            return (price * value) / 100

        return 0

    def calculate_fees(self, carts):
        for cart in carts:
            temp_total = cart["total"]
            for item in self.fees:
                min_price = item["eligible_transaction_volume"]["min_price"]
                max_price = item["eligible_transaction_volume"]["max_price"]
                fee = item["price"]
                check_min = temp_total >= min_price
                check_max = True if max_price is None else temp_total <= max_price
                if check_min and check_max:
                    temp_total += fee
                    break

            cart["total"] = temp_total

        return carts

    def apply_discount(self, carts, discounts):
        temp_carts = []
        for cart, discount in zip(carts, discounts):
            temp_carts.append(
                {"id": cart["id"], "total": int(cart["total"] - discount["discount"])}
            )

        return temp_carts

    def total(self):
        carts, discounts = self.total_by_quantity_price()
        carts = self.calculate_fees(carts)
        carts = self.apply_discount(carts, discounts)

        return {"carts": carts}
