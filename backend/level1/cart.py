from marshmallow import ValidationError

from common.cart import CartInterface
from common.schemas import CartResponseSchema


class CartOps(CartInterface):
    def total_by_quantity_price(self):
        carts = []

        articles = {article.id: article.price for article in self.articles}

        for cart in self.carts:

            total = sum(
                [articles[item.article_id] * item.quantity for item in cart.items]
            )

            try:
                result = CartResponseSchema().load(dict(id=cart.id, total=total))
            except ValidationError as err:
                raise err

            carts.append(result)

        return carts

    def apply_discount(self):
        pass

    def calculate_fees(self):
        pass

    def total(self):
        response = {"carts": self.total_by_quantity_price()}
        return response
